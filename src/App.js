import { useState, useContext, useReducer } from 'react'

import {Helmet} from "react-helmet";
import { Container } from '@mui/material';

import './App.css';

import Router from './Router';
import Footer from './components/Footer';
import HuntersContext from './context/context';
import HuntersReducer from './context/reducer';

export default function App() {
  const initialContext = useContext(HuntersContext);
  const [state, dispatch] = useReducer(HuntersReducer, initialContext);

  return (
    <Container maxWidth={false}>
      <Helmet>
          <title>Hunters exam by Paz</title>
      </Helmet>
      <HuntersContext.Provider value={{state, dispatch}}>
        <Router />
        <Footer/>
      </HuntersContext.Provider>
    </Container>
  )
}