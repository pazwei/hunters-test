import react from 'react';
import { useNavigate } from "react-router-dom";

import { Typography, ImageList, ImageListItem, ImageListItemBar } from '@mui/material';

export default function TopShowsCategoryScroller({name, shows}){
    const navigate = useNavigate();
    //TODO: in order to create infinite scroll, listen to onScroll event on the image list
    //if the scroll is at 80% that re-append the shows to the existing array
    function handleOnScroll(e){
        // console.log(e);
        console.log(window.document.body.scrollWidth, e.target.offsetWidth, e.target.scrollLeft);
        // console.log(e.target.scrollLeft);
    }

    function handleShowClick(show){
        navigate(`/details/${show.id}`);
    }

    return (
        <div className="scroller">
            <Typography variant="h3">Top {shows.length} {name}</Typography>
            <ImageList
            sx={{
                gridAutoFlow: "column",
                gridTemplateColumns: "repeat(auto-fit, minmax(240px,1fr)) !important",
                gridAutoColumns: "minmax(240px, 1fr)"
            }}
            gap={80}
            onScroll={(e) => handleOnScroll(e)}
            >
            {shows.map((show) => (
                <ImageListItem key={show.id} onClick={() => handleShowClick(show)} style={{cursor:'pointer'}}>
                    <img src={show.image.medium} alt={show.image.medium} style={{maxWidth:'240px'}}/>
                    <ImageListItemBar title={show.name}/>
                </ImageListItem>
            ))}
            </ImageList>
        </div>
    )
}