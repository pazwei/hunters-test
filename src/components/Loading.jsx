import react from 'react';

import {CircularProgress} from '@mui/material';
export default function Loading(){  
    return(
        <>
            {/* like netflix */}
            <CircularProgress size={120} className="spinner" color="error"/> 
        </>
    )
}