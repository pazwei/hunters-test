import {useContext} from 'react';
import axios from 'axios';
import _ from 'lodash';

import HuntersContext from '../context/context';

export default function useApi(){
    const {state, dispatch} = useContext(HuntersContext);

    async function getTopShowsByGenre(max){
        let cached = getCache('top'+max);
        if(!cached){
            try{
                let shows = await getShowList();
                let withoutNulls = _.filter(shows, (item) => item.rating.average);
                let ordered = _.orderBy(withoutNulls, ['rating.average'],['desc']);
                let grouped = {};

                //TODO: refactor ASAP HIGHLY INEFFICIENT!!!
                for(let show of ordered){
                    for(let genre of show.genres){
                        if(!grouped[genre]){
                            grouped[genre] = [show];
                        }else if(grouped[genre].length > max - 1){ //Once the list is sorted by rating, there is no need to push more items than asked. I definitly know that's in the correct order
                            continue;
                        }else{
                            grouped[genre].push(show);
                        }
                    }
                }

                let res = [];

                //Convert the object to array since dictionaries may NOT ALPHABETICAL RESERVE ORDER
                for(let key of Object.keys(grouped)){
                    res.push({
                        name:key,
                        shows:grouped[key]
                    });
                }

                res = _.orderBy(res, ['name'],['asc']);
                setCache('top'+max, res);                
                return res;
            }catch(e){
                console.log('getTopShowsByGenre error...');
                console.log(e);
                //TODO: handle error more nicely.
            }
        }else{
            return cached;
        }
    }

    async function getShowList(){
        let cached = getCache('showList');
        if(!cached){
            try{
                console.log('fetching data');
                let response = await axios.get('/shows');
                setCache('showList', response.data);
                return response.data;
            }catch(e){
                console.log('getShowList error...');
                console.log(e);
                //TODO: handle error more nicely.
            }
        }else{
            return cached;
        }
    }

    function getCache(key){
        let data = sessionStorage.getItem(key);
        if(!data){
            return null;
        }
        let cached = JSON.parse(data);
        console.log(cached);
        if(new Date(cached.exp) > new Date()){
            return cached.data;
        }
        return null;
    }

    function setCache(key, data){
        sessionStorage.setItem(key, JSON.stringify({exp: (new Date()).getTime() + 5 *  60 * 1000, data: data}));
    }

    async function getShow(showId){
        //Not cached, I assume the data is "hot"
        try{
            let res = await axios.get(`/shows/${showId}`);
            return res.data;
        }catch(e){
            console.log('getShow error...');
            console.log(e);
            //TODO: handle error more nicely.
        }
    }

    return { getShowList, getTopShowsByGenre, getShow};
}