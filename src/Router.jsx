import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Loading from './components/Loading';
import Top10 from './pages/Top10';
import Details from './pages/Details';
import Error from './pages/Error';


// const Top10 = React.lazy(() => import('./pages/Top10'));
// const Details = React.lazy(() => import('./pages/Details'));
// const Error = React.lazy(() => import('./pages/Error'));

export default function Router() {
    return (
        <BrowserRouter>
            <React.Suspense fallback={<Loading/>}>
                <Routes>
                    <Route path="/" element={<Top10 />} />
                    <Route path="/details/:id" element={<Details />} />
                    <Route path="*" element={<Error />} />
                </Routes>
            </React.Suspense>
        </BrowserRouter>
    )
}