export default function HunterReducer(state, action){
    switch(action.type){
        case 'SET_LOADING':
            return {...state, loading:action.payload};
        case 'SET_USER':
            return {...state, user:action.payload};
        default:
            return state;
    }
}