import { createContext } from 'react';
import initialContext from './initialContext';

let HuntersContext = createContext(initialContext);

export default HuntersContext;