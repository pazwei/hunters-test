import react, { useState, useEffect, useContext } from 'react';

import { Typography } from '@mui/material';

import Top10CategoryScroller from '../components/Top10CategoryScroller';
import Loading from '../components/Loading';
import useApi from '../hooks/useApi';

export default function Top10(){
    const { getTopShowsByGenre } = useApi();
    const [top10ShowsByCategory, setTop10ShowsByCategory] = useState(null);

    useEffect(() => {
        // get the list of top 10 shows grouped by categories from hook, only once when component mounted
        async function fetchData(){
            let data = await getTopShowsByGenre(10);
            setTop10ShowsByCategory(data);
        }
        fetchData();
    },[]);

    return(
        <>
            {!top10ShowsByCategory ? 
                <Loading /> :
                <>
                    <Typography variant="h1">TV Shows Library</Typography>
                    {top10ShowsByCategory.map((genre) => <Top10CategoryScroller key={genre.name} {...genre} />)}
                </>
            }
        </>
    );
}