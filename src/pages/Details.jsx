import react,{useState, useEffect} from 'react';
import { useParams, useNavigate } from "react-router-dom";

import { Typography, Grid, Rating, Button } from '@mui/material';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';

import Loading from '../components/Loading';
import useApi from '../hooks/useApi';

export default function Details(){
    const { getShow } = useApi();
    const [show, setShow] = useState(null);
    let { id } = useParams();
    const navigate = useNavigate();

    useEffect(()=>{
        async function fetchData(){
            let data = await getShow(id);
            setShow(data);
        }

        fetchData();
    },[]);

    function handleBack(){
        navigate('/');
    }

    return(
        <>
            {!show ? 
                <Loading /> :
                <>
                    <Typography variant="h1">{show.name}</Typography>
                    <br/>
                    <br/>
                    <Button startIcon={<ArrowBackIosIcon />} variant="contained" onClick={handleBack}>Back</Button>
                    <br/>
                    <br/>
                    <Grid container>
                        <Grid item xs={4}>
                            <div className="detailsContentSection">
                                <Typography variant="h6">Rating: <Rating size="large" precision={0.25} name="read-only" value={show.rating.average / 2} readOnly /> ({show.rating.average} / 10)</Typography>
                            </div>

                            <div className="detailsContentSection">
                                <Typography variant="h6">Summary: </Typography>
                                <div dangerouslySetInnerHTML={{__html:show.summary}}></div>
                            </div>
                        </Grid>
                        <Grid item xs={8}>
                            <img src={show.image.original} alt={show.image.original} className="detailsImage"/>
                        </Grid>
                    </Grid>
                </>
            }
        </>        
    )
}